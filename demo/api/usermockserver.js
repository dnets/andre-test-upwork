import express from 'express'
import bodyParser from 'body-parser'

// Create app
const app = express()
app.use(bodyParser.json())

// For demo purposes
app.use(function(req,res,next){setTimeout(next,1000)});

let bands = [
      { id: 1, name: 'Black Sabbath', rating: 5 },
      { id: 2, name: 'Electric Light Orchestra', rating: 4 },
      { id: 3, name: 'Thin Lizzy', rating: 3 },
      { id: 4, name: 'Deep Purple', rating: 5 },
      { id: 5, name: 'GG Allin', rating: 5 },
      { id: 6, name: 'Bachmann-Turner Overdrive', rating: 5 },
      { id: 7, name: 'Genesis', rating: 5 },
      { id: 8, name: 'Pink Floyd', rating: 5 },
      ];


// -- Routes --
app.get('/bands', (req, res) => {
  res.json({
      bands: bands
  })
})
app.post('/bands', (req, res) => {
  const { newband } = req.body
  if (newband) {
    let band = newband;
    band.id = bands.length+1;
    bands.push(band);
    res.json({status: 200});
  } else {
    res.json({status: 500});
  }
})
app.put('/bands', (req, res) => {
    const { bandId, newName, rating } = req.body.data;
    const findById = (element) => element.id == bandId;
    bands[bands.findIndex(findById)].rating = rating;
    res.json({status: 200, band: bands[bandId]});
});
app.delete('/bands', (req, res) => {
    const { bandId } = req.body;
    const findById = (element) => element.id == bandId;
    bands.splice(bands.findIndex(findById), 1);
    res.json({status: 200});
});


// -- export app --
export default {
  path: '/user',
  handler: app
}
